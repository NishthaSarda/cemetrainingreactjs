import React from "react";

const EmployeeName = (props) => <h1>{props.name}</h1>;

export default EmployeeName;
