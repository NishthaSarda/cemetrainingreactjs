import React, { useState, useEffect } from "react";
import Employee from "./Employee";
import "./App.css";
import axios from "axios";
import { BrowserRouter as Router, Switch, Link, Route } from "react-router-dom";

import AddEmployeeForm from "./AddEmployeeForm";

const App = () => {
  const [employeesData, setEmployeesData] = useState([]);

  useEffect(() => {
    axios.ge;
    axios.get("http://localhost:8080/api/all").then((resp) => {
      setEmployeesData(resp.data);
    });
  });

  return (
    <Router>
      <div className="app">
        <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/add">AddEmployee</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route path="/add">
            <AddEmployeeForm />
          </Route>
          <Route exact path="/">
            <div className="container">
              <div className="row">
                {employeesData.map((employeeData) => (
                  <Employee
                    key={employeeData.id}
                    employeeData={employeeData}
                  ></Employee>
                ))}
              </div>
            </div>
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;

// [
//   {
//     id: 11,
//     email: "hjdgfj@gmi.com",
//     salary: 4440,
//     name: "Nishtha",
//     age: 40,
//   },

//   {
//     id: 10,
//     email: "anbcj@we.com",
//     salary: 2220,
//     name: "Nish",
//     age: 20,
//   },

//   {
//     id: 12,
//     email: "cccfj@gmi.com",
//     salary: 12330,
//     name: "Nisha",
//     age: 30,
//   },
// ];
