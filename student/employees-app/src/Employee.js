import React, { useState } from "react";
import EmployeeName from "./EmployeeName";
import EmployeeDetails from "./EmployeeDetails";
import ShowHideButton from "./ShowHideButton";

const Employee = ({ employeeData }) => {
  const [visible, setVisibity] = useState(true);
  return (
    <div className="col-md-4">
      <div className="card mb-4 box-shadow" style={{ width: "18rem" }}>
        <div className="card-body">
          <EmployeeName name={employeeData.name}></EmployeeName>
          <EmployeeDetails
            employeeData={employeeData}
            visible={visible}
          ></EmployeeDetails>
          <ShowHideButton toggle={setVisibity} visible={visible} />
        </div>
      </div>
    </div>
  );
};

export default Employee;
