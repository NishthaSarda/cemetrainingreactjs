import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";

const AddEmployeeForm = () => {
  const [id, setId] = useState("");
  const [name, setName] = useState("");
  const [salary, setSalary] = useState("");
  const [email, setEmail] = useState("");
  const [age, setAge] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(id, name, salary, email, age);
    axios
      .post("http://localhost:8080/api/add", {
        name: name,
        id: id,
        salary: salary,
        email: email,
        age: age,
      })
      .then(() => {
        console.log("employee Added ");
      });
  };

  return (
    <div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
      <h3>Add Album</h3>
      <form onSubmit={handleSubmit}>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label>Name</label>
            <input
              id="name"
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>

          <div className="form-group col-md-5">
            <label>ID</label>
            <input
              id="id"
              type="text"
              value={id}
              onChange={(e) => setId(e.target.value)}
            />
          </div>

          <div className="form-group col-md-5">
            <label>Salary</label>
            <input
              id="salary"
              type="text"
              value={salary}
              onChange={(e) => setSalary(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label>Email</label>
            <input
              id="email"
              type="text"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label>Age</label>
            <input
              id="age"
              type="text"
              value={age}
              onChange={(e) => setAge(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <input
              type="submit"
              value="Add Emaployee"
              className="btn btn-outline-secondary"
            />
          </div>
        </div>
      </form>
    </div>
  );
};

export default AddEmployeeForm;
