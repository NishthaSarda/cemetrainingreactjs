import React from "react";

const EmployeeDetails = (props) => {
  return (
    props.visible && (
      <div>
        <h5>
          <b>Id: </b>
          {props.employeeData.id}
        </h5>
        <h5>
          <b>Email: </b> {props.employeeData.email}
        </h5>
        <h5>
          <b>Salary: </b>
          {props.employeeData.salary}
        </h5>
        <h5>
          <b>Age: </b>
          {props.employeeData.age}
        </h5>
      </div>
    )
  );
};

export default EmployeeDetails;
