import React from "react";

class Greeting extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
  }
  render() {
    return <h1>Greeting Component{this.props.name}</h1>;
  }
}

export default Greeting;
