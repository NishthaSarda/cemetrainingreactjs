import React from "react";

class Person extends React.Component {
  constructor(props) {
    super(props);
    console.log(props);
    this.state = { color: "red", age: 12 };
  }
  grow = () => {
    this.setState({ age: this.state.age + 1 });
  };
  render() {
    return (
      <div>
        <h1>
          person component {this.props.name} likes {this.state.color} and he is
          of age {this.state.age}
        </h1>
        <button onClick={this.grow}>chnage</button>
      </div>
    );
  }
}

export default Person;
