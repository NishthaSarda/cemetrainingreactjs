import React, { useEffect, useState } from "react";
import ReactDOM from "react-dom";
import Greeting from "./greeting.js";
import Person from "./person.js";

function Greet(props) {
  console.log(props.name);
  return <h1>{props.name}</h1>;
}

const PerosnFunction = (props) => {
  const [height, setHeight] = useState(185);
  useEffect(() => {
    console.log("Load on screen ");
  }, [height]);

  return (
    <div>
      <h1>
        person component {props.name} with height {height}.
      </h1>
      <button onClick={() => setHeight(height + 1)}>Increase</button>
    </div>
  );
};

ReactDOM.render(
  <div>
    {" "}
    hello <Greet name="Nishtha"></Greet>
    hiiiiii <Greeting name="Sarda"></Greeting>
    lets see this <Person name="NEwperson"></Person>
    functional comp <PerosnFunction name="functional comp"></PerosnFunction>
  </div>,
  document.querySelector("#root")
);
