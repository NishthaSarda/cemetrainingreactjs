import React from "react";
import ReactDOM from "react-dom";

import App from "./components/App";

import { Provider } from "react-redux";
import { createStore } from "redux";

const albumsReducer = (state = [], action) => {
  return state;
};

const store = createStore(albumsReducer);
console.log("store is", store.getState());
ReactDOM.render(
  <Provider>
    <App />
  </Provider>,
  document.getElementById("root")
);
