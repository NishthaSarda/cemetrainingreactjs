import React, { useState } from "react";
import axios from "axios";
import { useHistory } from "react-router-dom";

const CreateAlbumForm = ({ fetchAlbum, setFetchAlbum }) => {
  const history = useHistory();
  const [title, setTitle] = useState("");
  const [artist, setArtist] = useState("");
  const [price, setPrice] = useState("");
  const [tracks, setTracks] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(title, artist, price, tracks);

    axios
      .post("http://localhost:8088/albums", {
        title: title,
        artist: artist,
        price: price,
        tracks: tracks,
      })
      .then(() => {
        console.log("Alubm Added ");
        setFetchAlbum(!fetchAlbum);
        history.push("/");
      });
  };

  return (
    <div className="container" style={{ marginTop: 10, marginBottom: 150 }}>
      <h3>Add Album</h3>
      <form onSubmit={handleSubmit}>
        <div className="form-row">
          <div className="form-group col-md-5">
            <label>Title</label>
            <input
              id="title"
              type="text"
              value={title}
              onChange={(e) => setTitle(e.target.value)}
            />
          </div>

          <div className="form-group col-md-5">
            <label>Artist</label>
            <input
              id="artist"
              type="text"
              value={artist}
              onChange={(e) => setArtist(e.target.value)}
            />
          </div>

          <div className="form-group col-md-5">
            <label>Price</label>
            <input
              id="price"
              type="text"
              value={price}
              onChange={(e) => setPrice(e.target.value)}
            />
          </div>
          <div className="form-group col-md-5">
            <label>Tracks</label>
            <input
              id="tracks"
              type="text"
              value={tracks}
              onChange={(e) => setTracks(e.target.value)}
            />
          </div>
        </div>
        <div className="form-row">
          <div className="form-group col-md-5">
            <input
              type="submit"
              value="Create Album"
              className="btn btn-outline-secondary"
            />
          </div>
        </div>
      </form>
    </div>
  );
};

export default CreateAlbumForm;
