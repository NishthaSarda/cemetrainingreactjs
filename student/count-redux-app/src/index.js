import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, combineReducers } from "redux";

import App from "./App";

// state is always undefined at the start
const countReducer = (state = 0, action) => {
	// if (state === undefined) {
	// 	state = 1;
	// 	return state;
	// }
	console.log(`received ${action.type} dispatch in countReducer`);
	if (action.type === "ADD_COUNT") {
		return action.payload; // returns state = 1 according to the payload
	}
	if (action.type === "DECREMENT_COUNT") {
		return state - 1;
	}

	return state;
};

const weightReducer = (state = 100, action) => {
	console.log(`received ${action.type} dispatch in weightReducer`);
	if (action.type === "ADD_WEIGHT") {
		return action.payload; // returns state = 1 according to the payload
	}
	return state;
};

const heightReducer = (state = 185, action) => {
	console.log(`received ${action.type} dispatch in heightReducer`);
	if (action.type === "ADD_HEIGHT") {
		return action.payload; // returns state = 1 according to the payload
	}
	return state;
};

const reducers = combineReducers({
	count: countReducer,
	weight: weightReducer,
	height: heightReducer,
});

const store = createStore(reducers);
console.log("store is ", store.getState());
ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>,
	document.getElementById("root")
);
