import React from "react";
import { connect } from "react-redux";

const Height = (props) => (
	<div style={{ border: "1px solid black" }}>
		<p>Height is {props.height}</p>
		<p>Count is {props.count} from Height component</p>
	</div>
);

const mapStateToProps = (state) => {
	return {
		height: state.height,
		count: state.count,
	};
};

export default connect(mapStateToProps)(Height);
